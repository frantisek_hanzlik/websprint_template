import React from "react"

export class Layout extends React.Component {
	public render() {
		return (
			<>
				<div>Common header</div>
				{this.props.children}
			</>
		)
	}
}
