title Stack setup
echo installing packages in the project root..
start npm i
cd packages/frontend
echo installing packeges on the frontend..
start npm i
echo setting up symlinks (1/2) removing broken symlinks
del pages
echo setting up symlinks (2/2) creating symlinks
mklink /J pages "src/main/typescript/nextjs_routes/"
cd ..\..
echo all done! press any key to start the stack :-)
pause
npm run start_development